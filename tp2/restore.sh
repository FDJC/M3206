#!/bin/bash

path=$(readlink -f $1)    			#chemin absolu où le fichier était initialement

# readlink -f permet d'avoir le chemin absolu du répertoire que l'on va sauvegarder

rm -rf $1			#on supprime le répertoire précédent	

#on stocke dans la variable sortie le nom de l'archive de sauvegarde la plus récente:

sortie=$(for fichier in `ls /home/backup | grep $(basename $1).tar.gz`
#pour chaque fichier qui correspond au nom du répertoire

do	#on va vérifier pour chaque archive que le chemin correspond au chemin du répertoire que l'on veut restaurer 
	if [ $(tar -xOf /home/backup/$fichier $(basename $1)/chemin.txt) = $path ]
#la commande tar permet d'extraire l'archive de sauvegarde l'option -O permet de renvoyer le résultat dans la sortie standard
	then
		echo $fichier #on affiche tous les fichiers qui correspond à ce pattern
	fi
done | sort -r | head -1)

#sort -r permet de trier la sortie dans le sens inverse pour avoir le fichier le plus récent en premier

#head -1 permet de ne garder que le fichier le plus récent

tar xvzf /home/backup/$sortie -C $(dirname $path)

echo $sortie




