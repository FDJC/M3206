#!/bin/bash

if [ "$0" == "./monjeu.sh" ]; then
    echo "Le script est nommé et lancé correctement"
	if [ -e "test" ]; then
		echo "test a été crée"	
		droit=$(stat -c %A test) #la commande stat -c %A permet d'avoir la veleur des droits du fihier en chain de caractère
		bonDroit="-rw-r-----"
			if [ $droit = $bonDroit ]; then	#on vérifie que le fichier a les droits -rw-r----- soit 640 en octal
				echo "test a les bons droits"
				
				editeurDef="/usr/bin/vim" #on donne à une variable le chemin de l'éditeur que l'on veut par défaut
				if [ $EDITOR = $editeurDef ]; then
					echo "vim est mis comme éditeur par défaut"
					shaCompteur=$(shasum compteur | awk '{print $1}') 
					if [ $shaCompteur == 234e7e9c9c8490946d3e8c2a01bff41e9acce269 ]; then
						echo "le fichier compteur a correctement été crée"

						if [ -e "nbligne" ]; then #on vérifie d'abord si le fichier nbligne existe
							echo "le fichier nbligne est crée"
							shaNbligne=$(shasum nbligne | awk '{print $1}')
							if [ $shaNbligne == 5e8e15f3df1dc69e865117a99a9e804f2d13e03f ]; then
								echo "le fichier nbligne contient le bon nombre"

							
								if [ -e "nbls" ]; then #on vérifie d'abord si le fichier nbls existe
									echo "le fichier nbligne est crée"
									shaNbls=$(shasum nbls | awk '{print $1}')
									if [ $shaNbls == 33ac14b607229120233d7251b8a303038444fe18 ]; then
										echo "le fichier nbls contient le bon nombre"
										if [ -e "nbcommande" ]; then #on vérifie d'abord si le fichier nbcommande existe
											echo "le fichier nbcommande est crée"
											shaNbcommande=$(shasum nbcommande | awk '{print $1}')
											if [ $shaNbcommande == f9685598ec99389b73548f6a4ff055a2b17da137 ]; then
												echo "le fichier nbcommande contient le bon nombre"

											else
												echo "entrez le nombre de commande que l'on a tapé entre le 12Nov et le 15Nov dans le fichier nbcommande"
												exit 1
											fi						


	
										else
											echo "créez le fichier nbcommande qui contient le nombre de commandes tapées entre le 12 Nov et le 15 Nov"
											exit 1
										fi					

										
											




									else
										echo "entrez le nombre de fois que l'on a tapé ls dans le fichier nbls"
										exit 1
									fi						


	
								else
									echo "créez le fichier nbls qui contient le nombre de ligne de l'historique"
									exit 1
								fi					



							else
								echo "entrez le nombre de ligne du fichier my_history dans nbligne"
								exit 1
							fi						


	
						else
							echo "créez le fichier nbligne qui contient le nombre de ligne de l'historique"
							exit 1
						fi					

					else
						echo "Réalisez un fichier compteur qui contient les nombres de 1 à 1000 (un nombre par ligne)"
					exit 1
					fi
					

				else
					echo "Mettez vim comme éditeur par défaut"
					exit 1
				fi

			else
				echo "Donnez le droit rw-r----- au fichier test"
			fi

	else
		echo "Créez le fichier test"
		exit 1
	fi
else
    echo "Modifier le nom de ce script en monjeu.sh et exécuter le avec ./monjeu.sh"
    exit 1
fi

