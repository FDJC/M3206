#!/bin/bash

path=$(readlink -f $1)                          #chemin absolu du fichier à sauvegarder

# readlink -f permet d'avoir le chemin absolu du répertoire à sauvegarder

echo $path > $path/chemin.txt
#on met le chemin du répertoire à sauvegarder dans chemin.txt qui est dans l'archive

chemArchive=/home/backup/$(date +%Y_%m_%d_%H_%M)_$(basename $1).tar.gz

#chemArchive correspond au chemin où l'achive va être stocké, ici /home/rt
#basename permet d'avoir le nom de base du fichier et pas avoir le chemin complte derière

tar cvzf $chemArchive $1

#tar permet de compresser/décompresser des fichiers
#cvzf permet dans l'ordre de "compresser" "en mode verbeux" "en gzip" 
#date permet d'avoir la date
#%Y pour l'année
#%m pour le mois
#%d pour le jour
#%H pour l'heure
#%M pour les minutes


