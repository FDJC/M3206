#!/bin/bash

p="first"	#variables qui contiennent first et last pour après comparer
d="last"



if [ $1 != $p ] && [ $1 != $d ]	#si l'utilisateur n'entre ni last ni first
then
echo "First argument should be first or last."		#on annonce dans ce cas la bonne syntaxe à entrer
echo "usage: ./datecomm.sh [first|last] command file"
exit 1
fi


if [ ! -e $3 ]	#on vérifie la non présence du fichier donné en paramètre
then
echo "Le fichier "$3" n'existe pas." #on annonce à l'utilisateur que son fichier n'existe pas
echo "usage: ./datecomm.sh [first|last] command file"
exit 1
fi


if [ -z $1 ] || [ -z $2 ] || [ -z $3 ]	#on vérifie si les variables sont vides avec l'option -z pour voir si l'utilisateur n'a pas entré une commande incomplète
then
echo "The command takes 3 arguments." #on annonce à l'utilisateur qu'il doit y a voir dans sa commande 3 arguments ni plus ni moins
echo "usage: ./datecomm.sh [first|last] command file"
exit 1
fi

nbOccurence=$(cat my_history | awk '{print $2}' | grep $2 | cut -d ';' -f2 | wc -l) 
#on regarde le nombre d'occurence de la commande passée en paramètre
#on utilise wc -l pour compter le nombre de ligne de la sortie


if [ $nbOccurence -eq 0 ] 		#on regarde si on a des occurences dans le fichier, si on en a pas, on dit à l'utilisateur que la commande n'apparait pas dans l'historique 
then
echo "The history does not contain "$2" command"
exit 1
fi



if [ $1 = $p ]			#on vérifie si l'utilisateur a entré first pour connaitre la première fois
then

tstamp=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | head -1)

fi

if [ $1 = $d ]			#on vérifié si l'utilisateur a entré last pour connaitre la dernière fois 
then

tstamp=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | sort -r | head -1)

#on rajoute sort -r pour inverser la sortie , afin d'afficher la dernière fois en premier
#puis, on fait un "head -1" pour n'afficher que la première entrée qui correspond
#désormais à celle qui a le timestamp le plus élevé et donc la plus récente



fi

date -d @$tstamp
#on affiche le timestamp de manière humainement compréhensible

