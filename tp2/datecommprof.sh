#!/bin/bash

p="first"			#on stocke dans deux variable first et last
d="last"


if [ $1 = $p ]			#on vérifie si l'utilisateur a entré first pour connaitre sa première fois
then

tstamp=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | head -1)

fi

if [ $1 = $d ]			#on vérifié si l'utilisateur a entré last pour connaitre sa dernière fois 
then

tstamp=$(cat $3 | awk '{print $2}' | grep $2 | cut -d ':' -f1 | sort -r | head -1)

#on rajoute sort -r pour inverser la sortie , afin d'afficher la dernière fois en premier
#puis, on fait un "head -1" pour n'afficher que la première entrée qui correspond
#désormais à celle qui a le timestamp le plus élevé et donc la plus récente



fi

date -d @$tstamp
#on affiche le timestamp de manière humainement compréhensible

