#!/bin/bash

tstamp=$(cat $2 | awk '{print $2}' | grep $1 | cut -d ':' -f1 | head -1)

#on affiche le fichier donné en paramètre 
#awk '{print $2}' permet de n'afficher que la deuxième colonne
#grep $1 permet de ne garder que les lignes qui contiennent la commande donné en paramètre
#cut -d permet de couper la chaine de caractère où on a un ":"
#-f1 permet de garder la partie avant ":"
#head -1 permet d'avoir la première commande tapé

date -d @$tstamp	#on affiche la date à laquel on a tapé cette commande
#l'option -d suivi de @ permet de convertir un timestamp en date humainement compréhesible


