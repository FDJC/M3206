Contenu des cours, TDs et TPs du module M3206

TD1

Exercice 1

La commande git status retourne l'etat actuel du projet
avec les fichiers qui ont été modifiés

Après avoir crée TD1_Git.txt si on fait un git status, on remarque qu'il n'y 
a pas de modification

Après avoir effectué la commande git add TD1_Git.txt, on remarque qu'on a
"new file: TD1_Git.txt" qui nous montre qu'on a bien ajouté le fichier

Après avoir fait la commande commit, on remarque que git status retourne la 
même chose qu'au début

Exercice 2

La commande git log retourne ce que l'on a effectué comme publication

Exercice 3

si on effectue un git status, on remarque que l'on nous signale que le 
fichier TD1_Git.txt a été modifié

Après avoir utilisé la commande git checkout et git status, on remarque
que notre projet est revenu à son état précédent

Exercice 4

En faisant un git status on remarque que l'on nous affiche que
le fichier TD1_Git.txt a été modifié

Après avoir fait un git reset et un git checkout, si on vérifie l'état du projet
on remarque que le projet est revenu comme avant avec aucune modification

Exercice 5





