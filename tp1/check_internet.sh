#!/bin/bash
echo "[...] Checking internet connection [...]"

if ping -c 3 www.google.fr >/dev/null #on envoie la sortie standard de ping dans /dev/null
then
	echo "[...] Internet access OK [...]"
	exit 0    #pas d'erreurs
else
	echo "[/!\] Not connected to internet	[/!\]"
	echo "[/!\] Please check configuration	[/!\]"
	exit 1    #une erreur
fi

