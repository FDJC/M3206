#!/bin/bash
if /etc/init.d/ssh status 1>/dev/null 2>/dev/null
#on vérifie si le daemon tourne en regardant son status 
then
echo "[...] ssh: est installé	[...]"
else
echo "[/!\] ssh: le service n'est pas lancé	[/!\]"

echo "[/!\] ssh: lancement du service	[/!\]"
/etc/init.d/ssh start #on lance le daemon s'il n'est pas lancé
fi

