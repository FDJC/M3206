#!/bin/bash
HISTFILE=~/.bash_history
set -o history
#car history ne fonctionne pas en script, on charge donc le fichier .bash_history

history | awk '{print $2}' | sort | uniq -c | sort -nr | head -$1



#history=> permet d'avoir l'historique des commandes
#awk=> trie la sortie
#{print $2}=>n'affiche que la deuxième colonne
#sort=>permet de trier les commandes par ordre alphabérique
#uniq -c=> permet de compter le nomrbre d'occurence
#sort -n=>met dans l'ordre numérique croissant la sortie
#sort -r=>inverse l'ordre de la sortie
#head -10=>affiche les 10 premières lignes de la sortie

#$1 permet de prendre le premier argument donné quand on entre
#la commande
