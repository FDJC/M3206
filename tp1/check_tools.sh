#!/bin/bash
if dpkg --status git 2>/dev/null 1>/dev/null #on vérifie sit le paquet est installé et on renvoie les sorties vers /dev/null pour ne pas les avoir dans le terminal
then
echo "[...] git: installé	[...]"
else
echo "[/!\] git: pas installé	[/!\]"
fi

if dpkg --status tmux 2>/dev/null 1>/dev/null
then
echo "[...] tmux: installé	[...]"
else
echo "[/!\] tmux: pas installé	[/!\]"
fi

if dpkg --status vim 2>/dev/null 1>/dev/null
then
echo "[...] git: installé	[...]"
else
echo "[/!\] vim: pas installé	[/!\]"
fi

if dpkg --status htop 2>/dev/null 1>/dev/null
then
echo "[...] htop: installé	[...]"
else
echo "[/!\] htop: pas installé	[/!\]"
fi


