#!/bin/bash

if [[ $EUID -eq 0 ]]; 
#si l'utilisateur est root
then
	echo "[...] update database [...]"
	apt-get update -qq 2>/dev/null
#on renvoie la sortie d'erreur
#et on met le apt-get en silencieux avec qq 
	echo "[...] upgrade system  [...]"
	apt-get upgrade -qq 2>/dev/null
	exit 0
else
	echo "[/!\] Vous devez être super-utilisateur [/!\]"
	exit 1
fi

