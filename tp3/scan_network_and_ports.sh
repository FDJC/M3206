#!/bin/bash


echo "The following IP address are responding"



for adresse in $(nmap -sn $1 | grep "report for" | awk '{print $5}')



#nmap est un utilitaire qui permet de scanner le réseau
#-sn spécifié à nmap de na pas scanner les ports mais uniquement les adresses IP
#grep "report for" permet d'afficher les lignes relatives à une réponse de l'adresse iP
#awk '{print $5}' permet de récupérer l'adresse ip de l'hôte qui répond

#pour chaque adresse qui répond dans la plage spécifié, faire:
do

echo $adresse

echo "	- open ports are: "$(nmap $adresse | grep open | awk 'BEGIN {FS= "/" } ; {print $1}'| tr '\n' ' ')


#on va scan chaque adresse et regarder avec grep les ports ouverts
#puis on va changer le delimiter de awk pour afficher uniquement le numéro des ports
#tr va nous permettre de remplacer les \n de la sortie qui sont des retours à la ligne en espaces pour avoir les numéro de ports sur la même ligne

done



