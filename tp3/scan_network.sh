#!/bin/bash


echo "The following IP address are responding"


adresse=$(ip route show | grep "scope link  src" | awk '{print $1}')

#adresse IP du réseau sur lequel la machine est

#on utilise la commande ip route show pour avoir la table de routage
#grep "scope link src" permet d'avoir la table de routage pour le réseau local
#awk '{print $1}' permet d'avoir uniquement l'adresse au format CIDR pour la sortie


nmap -sn $adresse | grep "report for" | awk '{print $5}'

#nmap est un utilitaire qui permet de scanner le réseau
#-sn spécifié à nmap de na pas scanner les ports mais uniquement les adresses IP
#grep "report for" permet d'afficher les lignes relatives à une réponse de l'adresse iP
#awk '{print $5}' permet de récupérer l'adresse ip de l'hôte qui répond




