#!/bin/bash
adIP=$(ifconfig eth0 2>/dev/null | grep "inet addr" | awk '{print $2}' | awk 'BEGIN {FS = ":" } ; {print $2}')

#on va stocker dans une variable l'adresse IP de la machine:
#ifconfig eth0 permet d'avoir la configuration réseau de la machine sur l'interface eth0
#2>/dev/null permet de rediriger la sortie d'erreu si l'interface eth0 n'existe pas
#grep "inet addr" permet de mettre en sortie la ligne qui contient l'adresse IP
#awk '{print $2}' permet de retenir la 2ème colonne que retourne la comamnde
#awk 'BEGIN {FS = ":" } ; {print $2}' change le caractère délimiteur en ":" et affiche la colonne de droite qui correspond à l'adresse IP

servDNS=$(cat /etc/resolv.conf | awk '{print $2}' | head -1)

#on lit le fihier /etc/recolv.conf qui contient les adresses des DNS
# on utilise awk '{print $2}' pour n'afficher que la 2ème colonne
#on utilise head -1 pour avoir la 1ère ligne et donc le serveur DNS primaire 


routeDef=$(route | grep default | awk '{print $2}')

#on lit les routes que la machine connait
#on cherche la ligne qui contient la route par défaut avec grep default
#on affiche la deuxième colonne de la sortie avec  awk '{print $2}', cette deuxième colonne correspond à l'adresse du gateway vers 0.0.0.0

hname=$(hostname)

#pour avoir le hostname de la machine, il suffit d'utiliser la commande hostname

upti=$(uptime | awk '{print $3}' | awk 'BEGIN {FS = "," } ; {print $1}')

#on utilise la commande uptime pour savoir le temps que cela fait que la machine tourne
#awk '{print $2}' permet de retenir la 2ème colonne que retourne la comamnde
#awk 'BEGIN {FS = "," } ; {print $1}' change le caractère délimiteur en "," et affiche la colonne de gauche pour éviter d'avoir une virgule


port21=$(nmap 127.0.0.1 | grep "22/tcp" | awk '{print $3}')

#variable pour savoir le service sur le port 22
#nmap permet de scanner ses propres port utilisés
#grep "22/tcp" permet de voir le service sur le port 22 de tcp
#awk '{print $3}' permet d'afficher la 3ème colonne de la sortie soit le nom du service

port80=$(nmap 127.0.0.1 | grep "80/tcp" | awk '{print $3}')


echo "hostname:			"$hname
echo "uptime:				"$upti
echo "adresse IP:			"$adIP
echo "route par défaut:		"$routeDef
echo "serveur dns:			"$servDNS
echo "service sur le port 22:		"$port21
echo "service sur le port 80:		"$port80


